/*
    ЗАДАЧІ:
    0. Стилізувати форму на CSS, HTM;
    1. Максимально 10 символів (показувати повідомлення "Перевищенна кількість символів");
    2. Якщо все вірно покзувати повідомлення "Форма успішно відправленна";
*/

document.addEventListener('DOMContentLoaded', function () {

    const input = document.getElementsByClassName('input');
    const error = document.getElementsByClassName('error');
    const submit = document.getElementById('submit');

    submit.addEventListener('click', function (e) {
        e.preventDefault();


        for (let i = 0; i < input.length; i++) {
            if (input[i].value === '') {
                error[i].innerHTML = 'Не допустимое пустое поле';
            } else {
                error[i].classList.add('done');
                error[i].innerHTML = 'Все верно';
            }

            setTimeout(() => {
                error[i].classList.remove('done');
                error[i].innerHTML = '';
            }, 1000);
        }

    })
});
